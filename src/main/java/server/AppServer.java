package server;


import com.hazelcast.core.HazelcastInstance;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import java.util.Map;
import java.util.Set;


public class AppServer {
    public static void main(String[] args) throws Exception {

        ServletContextHandler context =
                new ServletContextHandler(
                        ServletContextHandler.SESSIONS
                );

        context.setContextPath("/");

        HazelcastInstance instance = Client.getClient();
        Map<String,String> map = instance.getMap("employees");
        map.put("emp1","Steve");
        map.put("emp2","Chris");
        map.put("emp3","Katy");
        Set set  = map.entrySet();
        System.out.println(set);


        Server jettyServer = new Server(9000);
        jettyServer.setHandler(context);

        ServletHolder servletHolder =
                context.addServlet(
                org.glassfish.jersey.servlet.ServletContainer.class, "/*");

        servletHolder
                .setInitParameter(
                "jersey.config.server.provider.packages",
                "com.resource");

        try {
            jettyServer.start();
            jettyServer.join();

        } finally {
            jettyServer.destroy();
        }
    }
}