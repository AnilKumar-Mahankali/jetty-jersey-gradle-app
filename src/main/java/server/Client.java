package server;


import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

class Client {
     static HazelcastInstance getClient(){
        Config cfg = new Config();
        return Hazelcast.newHazelcastInstance(cfg);
    }
}
