package com.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("resource")
public class Resource {

    @GET
    @Path("hello")
    @Produces(MediaType.APPLICATION_JSON)
    public String squareRoot(){

        return "Hello, World.....!";
    }
}
